let table = document.createElement("table"); // create an element 'table'
table.classList.add("table"); // add the styles to the table
document.body.appendChild(table); // added the table to the end of the document

for (let i = 0; i < 30; i++) { // for a loop of 30 raws
    let tr = document.createElement("tr"); // create a raw
    table.appendChild(tr); // added the raw to the table
    for (let j = 0; j < 30; j++) { // for a loop of 30 cells
        let td = document.createElement("td"); // create a cell
        td.classList.add("cell-space"); // add the styles to the cell
        tr.appendChild(td); // added the cell to the raw
    }
}

document.addEventListener("click", (e) => { 
    if (e.target.tagName === 'TD') { // if the target of event == 'TD'
        console.dir(e.target);
        e.target.classList.toggle("cell-space_painted"); // switch to the class 'painted'
    } else {
        let painted = document.querySelectorAll(".cell-space"); // select all cells to be marked as 'painted'
        console.log(painted);
        painted.forEach((item) => { // for each such element
            item.classList.toggle("cell-space_painted"); // switch to the class 'painted'
            item.classList.toggle('cell-space_darkened'); // switch to the class 'darkened'
        });
    }
});